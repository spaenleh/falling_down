/*
 * Falling_Down Nintendo DS
 *
 * Created on: Nov 28, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */

#include <nds.h>
#include <stdio.h>

#include "graphics.h"
#include "timer_management.h"
#include "menu.h"
#include "parachute.h"
#include "barque.h"
#include "constantes.h"
#include "score.h"
#include "storage.h"
#include "sound.h"
#include "chrono.h"

int main(void) {

	init_storage();
	configureTimer();
	graphics_init();
	menu_init();
	sound_init();

	while(1){
		menu_state();
		if(!play)
			sound_playMusic();
		if(state == PLAYING){
			sound_stopMusic();
			para_update();
			score_update();
			oamUpdate(&oamMain);
		}
		swiWaitForVBlank();
	}
}
