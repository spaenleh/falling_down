/*
 * barque.c
 *
 *  Created on: Dec 4, 2019
 *  Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#include <nds.h>
#include <stdio.h>
#include "barque.h"
#include "bark.h"
#include "constantes.h"


// *************** GLOBAL VARIABLES ******************

//Pointer to the graphic buffer where to store the sprite
u16* gfx_barque;

static Barque barque;

// ********** DEFINITION FOR LOCAL FUNCTIONS ****************

//Modify the vertical position of the barque
void change_position_barque();

//Decide in which direction to move the barque
void barque_get_inc(int touch_pos);

//Move barque to the right
void barque_move_right();

//Move barque to the left
void barque_move_left();

// **************** CODE FOR FUNCTIONS *********************

void barque_config() {
	gfx_barque = oamAllocateGfx(&oamMain, SpriteSize_64x32, SpriteColorFormat_256Color);
	dmaCopy(barkTiles, gfx_barque, barkTilesLen);
}


void barque_init(){
	barque.x = SCREEN_WIDTH/2;
	barque.y = BARQUE_Y;
	barque.hide = false;
}

void barque_update(){
	scanKeys();
	int keys = keysHeld();
	if(keys & KEY_TOUCH){
		touchPosition touch;
		touchRead(&touch);
		if(touch.py>18*8){
			barque_get_inc(touch.px);
		}
	}

	if(keys & KEY_LEFT){
		barque_move_left();
	} else if(keys & KEY_RIGHT) {
		barque_move_right();
	}
	change_position_barque();
}

int barque_pos(){
	return barque.x;
}

// ************** CODE FOR LOCAL FUNCTIONS ******************

void barque_move_left(){
	barque.x = barque.x-BARQ_INC;
	if (barque.x < -LIM_LEFT)
		barque.x = -LIM_LEFT;
}

void barque_move_right(){
	barque.x = barque.x+BARQ_INC;
	if (barque.x > SCREEN_WIDTH-LIM_RIGHT)
		barque.x = SCREEN_WIDTH-LIM_RIGHT;
}

void barque_get_inc(int touch_pos){
	int error = barque.x-touch_pos+BARQ_OFS_MIDDLE;
	if (error < BARQ_INC && error > -BARQ_INC){
		return;
	}
	if (error > 0) {
		barque_move_left();
	}
	if (error <= 0) {
		barque_move_right();
	}
}

void change_position_barque(){
	oamSet(&oamMain, 		// oam handler
			BARQUE_INDEX,					// Number of sprite
			barque.x, barque.y, 	// Coordinates
			0,					// Priority
			0,					// Palette to use
			SpriteSize_64x32,			// Sprite size
			SpriteColorFormat_256Color,	// Color format
			gfx_barque,				// Loaded graphic to display
			-1,					// Affine rotation to use (-1 none)
			false,				// Double size if rotating
			barque.hide,				// Hide this sprite
			false, false,		// Horizontal or vertical flip
			false				// Mosaic
	);
}
