/*
 * parachute.h
 *
 *  Created on: Nov 26, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef PARACHUTE_H_
#define PARACHUTE_H_

#include <nds.h>
#include <stdio.h>

typedef struct{
	bool hide;
	int x;
	int y;
	u16* color;
}Parachute;

//Allocate space for the graphic to show in the sprite
void para_config();

//Initialize the parameters of the parachutes
//like their position, their color and if there are hide or not
void para_init();

//Call a local function that update the position of the parachutes that are not hide
void para_update();

//Unhide or activate parachute and initialize it
void new_para();

//Test if a parachute collide with the barque and return a boolean
bool para_collision();

#endif /* PARACHUTE_H_ */
