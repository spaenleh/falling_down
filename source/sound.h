/*
 * sound.h
 *
 *  Created on: Dec 28, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */

#ifndef SOUND_H_
#define SOUND_H_

//Bollean use to know is the music of the menu is playing
bool play;

//Initialize the sound library and load the music and the effect
void sound_init();

//Play the splash effect
void sound_playSplash();

//Play the music of the menu
void sound_playMusic();

//Stop the music of the menu
void sound_stopMusic();

#endif /* SOUND_H_ */
