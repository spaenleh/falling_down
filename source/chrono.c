/*
 * chrono.c
 *
 *  Created on: Dec 23, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#include <nds.h>
#include <stdio.h>

#include "chrono.h"
#include "timer_management.h"
#include "graphics.h"
#include "constantes.h"

// **************** CODE FOR FUNCTIONS ***************
void chrono_init(){
	timer1Start();
}

void chrono_stop(){
	timer1Stop();
}

void chrono_update(int min, int sec)
{
	int number;
	int x,y = 10;

	//MINUTES
	number = min;
	if(min > 59)
		min = number = -1;
	x = 7;
	if(min>=0)
		number = min/10;
	graphics_printDigit(number,x,y);
	x = 11;
	if(min>=0)
		number = min %10;
	graphics_printDigit(number,x,y);

	//COLON
	x = 15;
	graphics_printDigit(COL,x,y);

	//SECONDS
	number = sec;
	if(sec > 59)
		sec = number = -1;
	x = 17;
	if(sec>=0)
		number = sec / 10;
	graphics_printDigit(number,x,y);
	x = 21;
	if(sec>=0)
		number = sec % 10;
	graphics_printDigit(number,x,y);
}
