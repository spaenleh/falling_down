/*
 * graphics.h
 *
 *  Created on: Nov 26, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef GRAPHICS_H_
#define GRAPHICS_H_

//Activate and configure VRAM bank to work in background or sprite mode
void graphics_init(void);

//Configure and load the main_menu graphic on the main screen
void graphics_MAIN_menu(void);

//Configure and load the sub_menu graphic on the sub screen
void graphics_SUB_menu(void);

//Configure and load the main_playing graphic on the main screen and
//call some functions to configure and load the graphics for the sprites
void graphics_MAIN_playing();

//Configure and load the sub_playing graphic on the sub screen
void graphics_SUB_playing();

//Configure and load the main_difficulty graphic on the main screen
void graphics_SUB_dif(void);

//Configure and load the sub_difficulty graphic on the sub screen
void graphics_SUB_high(void);

//Configure and load the main_gameover graphic on the main screen
void graphics_MAIN_gameover(void);

//Load the sub_gameover graphic on the sub screen
void graphics_SUB_gameover(void);

//Cahnge the palette in order to highlight the selected menu item
void graphics_menu_select(int pos);

//Dislplay the number of lives given as a parameter
void graphics_SUB_lives(int remaining);

//Dislplay the number given as a parameter at the selected place
void graphics_printDigit(int number, int x, int y);


#endif /* GRAPHICS_H_ */
