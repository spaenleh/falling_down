/*
 * barque.h
 *
 *  Created on: Dec 4, 2019
 *  Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef BARQUE_H_
#define BARQUE_H_

typedef struct{
	int x;
	int y;
	bool hide;
}Barque;;

//Allocate space for the graphic to show in the sprite barque
void barque_config();

//Initialize the position of the barque and if it is hide or not
void barque_init();

//Call local functions that move the barque and update the sprite position
void barque_update();

//Return the current position of the barque in order to compare it with the parachute's position
int barque_pos();

#endif /* BARQUE_H_ */
