/*
 * parachute.c
 *
 *  Created on: Nov 26, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#include <nds.h>
#include <stdio.h>
#include "parachute.h"
#include "para.h"
#include "barque.h"
#include "constantes.h"
#include "sound.h"
#include "graphics.h"
#include "menu.h"


// *************** GLOBAL VARIABLES ******************

//Pointer to the graphic buffer where to store the sprite
u16* gfx[NB_C_PARA];

static Parachute tab_para[TAB_PARA_SIZE];
static int lives = 8;

// ********** DEFINITION FOR LOCAL FUNCTIONS ****************

//Hide a parachute and update its position
void hide(int nb);

//Update the horizontal and vertical position of a parachute
void change_position(int nb);

// **************** CODE FOR FUNCTIONS *********************

void para_config() {
	int i;
	for(i=0;i<NB_C_PARA;i++){
		gfx[i] = oamAllocateGfx(&oamMain, SpriteSize_32x32, SpriteColorFormat_256Color);
		dmaCopy(paraTiles+i*256, gfx[i], paraTilesLen/5);
	}
}

void para_init(){
	int i;
	for (i = 0; i < TAB_PARA_SIZE; i++) {
		tab_para[i].hide = true;
		tab_para[i].x = rand()%SCREEN_WIDTH;
		tab_para[i].y = 0;
		tab_para[i].color = gfx[rand()%4];
	}
	lives = MAX_LIVES;
}

void para_update(){
	int i;
	for(i=0;i<TAB_PARA_SIZE;i++){
		if(!tab_para[i].hide)
			change_position(i);
	}
}

void new_para(){
	int i=0;
	for(i=0;i<TAB_PARA_SIZE;i++){
		if(tab_para[i].hide){
			tab_para[i].hide = false;
			tab_para[i].x = rand()%SCREEN_WIDTH;
			tab_para[i].y = 0;
			tab_para[i].color = gfx[rand()%4];
			return;
		}
	}
}

bool para_collision(){
	int i;
	for(i=0;i<TAB_PARA_SIZE;i++){
		if(!tab_para[i].hide && tab_para[i].y >= (BARQUE_Y-Y_TOL) && tab_para[i].y <= (BARQUE_Y+Y_TOL)){
			if((tab_para[i].x+X_TOL) >= (barque_pos()+X_TOL) && (tab_para[i].x+X_TOL) <= (barque_pos()-X_TOL+SPRITE_WIDTH*2)){
				hide(i);
				return true;
			}
		}
	}
	return false;
}

// ************** CODE FOR LOCAL FUNCTIONS ******************

void hide(int nb){
	tab_para[nb].hide = true;
	change_position(nb);
}

void change_position(int nb){
	//Modify the vertical position of the sprite
	if(tab_para[nb].y < (BARQUE_Y+Y_TOL)){
		tab_para[nb].y += 1; // para is falling
	}
	else if(tab_para[nb].y < SCREEN_HEIGHT){
		// para was not catched
		tab_para[nb].color = gfx[4];
		tab_para[nb].y += 1;
	}
	else {
		sound_playSplash();
		lives-=1;
		graphics_SUB_lives(lives);
		tab_para[nb].hide = true;
		tab_para[nb].y = 0;
		if(lives<=0){
			menu_finished();
		}
	}

	//Modify the horizontal position of the sprite
	if(tab_para[nb].x < (SCREEN_WIDTH - X_TOL)){
		if (rand()%2) (tab_para[nb].x)++;
		else if (rand()%2) (tab_para[nb].x)--;
	} else {
		tab_para[nb].hide=true;
		tab_para[nb].y = 0;
		tab_para[nb].x=SCREEN_WIDTH/rand()%4+1;
	}

	oamSet(&oamMain, 			// oam handler
			nb+PARA_OAM_OFS,	// Number of sprite
			tab_para[nb].x, tab_para[nb].y, 	// Coordinates
			0,					// Priority
			0,					// Palette to use
			SpriteSize_32x32,			// Sprite size
			SpriteColorFormat_256Color,	// Color format
			tab_para[nb].color,			// Loaded graphic to display
			-1,					// Affine rotation to use (-1 none)
			false,				// Double size if rotating
			tab_para[nb].hide,	// Hide this sprite
			false, false,		// Horizontal or vertical flip
			false				// Mosaic
	);
}
