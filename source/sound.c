/*
 * sound.c
 *
 *  Created on: Dec 28, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */

#include <nds.h>
#include <stdio.h>
#include "sound.h"

#include <maxmod9.h>
#include "soundbank.h"
#include "soundbank_bin.h"

// *************** CODE FOR FUNCITONS ******************
void sound_init(){
	//Init the sound library
	mmInitDefaultMem((mm_addr)soundbank_bin);

	//Load module
	mmLoad(MOD_TWILIGHT);
	play = false;

	//Load effect
	mmLoadEffect(SFX_SPLASH2);
}

void sound_playSplash(){
	mmEffect(SFX_SPLASH2);
}

void sound_playMusic(){
	mmStart(MOD_TWILIGHT,MM_PLAY_LOOP);
	play = true;
}

void sound_stopMusic(){
	mmStop();
	play = false;
}
