/*
 * score.c
 *
 *  Created on: Dec 10, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */
#include <nds.h>
#include <stdio.h>

#include "score.h"
#include "constantes.h"
#include "parachute.h"
#include "graphics.h"

// ***************** GLOBAL VARIABLES **********************
static unsigned int score = 0;

// **************** CODE FOR FUNCTIONS *********************
void score_init(){
	score = 0;
	printScore(score);
}

void score_update(){
	if(para_collision()){
		score += 1;
		printScore(score);
	}
}

int get_score(){
	return score;
}

void printScore(int number){
	int x = DIGI_UNIT, new_nbr1 = number/10, new_nbr2 = number/100;
	if(number >= 0 && number <10){
		graphics_printDigit(number,x,SCORE_Y);
	} else if(number < 100){
		x = DIGI_DECI;
		graphics_printDigit(number-new_nbr1*10,x,SCORE_Y);
		x -= 4;
		graphics_printDigit(new_nbr1,x,SCORE_Y);
	} else {
		x = DIGI_CENTI;
		graphics_printDigit(number-(number-new_nbr2*100)/10*10-new_nbr2*100,x,SCORE_Y);
		x -= 4;
		graphics_printDigit((number-new_nbr2*100)/10,x,SCORE_Y);
		x -= 4;
		graphics_printDigit(new_nbr2,x,SCORE_Y);
	}
}
