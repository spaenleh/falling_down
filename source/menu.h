/*
 * menu.h
 *
 *  Created on: Dec 19, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef MENU_H_
#define MENU_H_

//Global variable use to know if the current state is the playing one or not
unsigned int state;

//Initialize the state to menu and load the correspondent backgrounds
void menu_init(void);

//Execute different local functions depending on which state is the current one
void menu_state(void);

//Stop the chrono, write the score and load the gameover background
void menu_finished(void);


#endif /* MENU_H_ */
