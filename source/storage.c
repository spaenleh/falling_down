/*
 * storage.c
 *
 *  Created on: Dec 27, 2019
 *      Authors: Basile Spealehauer and Marine Fumeaux
 */

#include "storage.h"
#include <stdio.h>
#include <sys/dir.h>
#include <fat.h>
#include <dirent.h>

#define FILE_NAME "/score.txt"

int score = 0;
int s_time = 0;


void init_storage(void){
	fatInitDefault();
	FILE* file = NULL;
	file=fopen(FILE_NAME,"r");
	if (file != NULL) {
		fscanf(file," %i",&score);
		fscanf(file," %i",&s_time);
		fclose(file);
	} else {
		score = 0;
		s_time = 0;
		file = fopen(FILE_NAME,"w+");
		fprintf(file,"%i %i\n",score,s_time);
	}
	return;
}

void write_score(int new_score, int new_time){
	if(new_time >= s_time){
		score = new_score;
		s_time = new_time;
		FILE* file = fopen(FILE_NAME,"w+");
		fprintf(file,"%d %d\n",new_score,new_time);
		fclose(file);
	}
}

int get_stored_score(){
	return score;
}

int get_stored_time(){
	return s_time;
}
