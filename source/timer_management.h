/*
 * timer_management.h
 *
 *  Created on: Nov 28, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */

#ifndef TIMER_MANAGEMENT_H_
#define TIMER_MANAGEMENT_H_

//Global variables modified depending on the selected difficulty
unsigned int speed_para, speed_barque;

//Initialize the timing variables and timer 0, 1 and 2
void configureTimer();

//Interruption subroutine of timer 0
void timer0ISR();

//Interruption subroutine of timer 1
void timer1ISR();

//Interruption subroutine of timer 2
void timer2ISR();

//Enable timer 1 and call a function to update the chronometer
void timer1Start();

//Disable the timer 1
void timer1Stop();

//Return the time when gameover
int get_time();

#endif /* TIMER_MANAGEMENT_H_ */
