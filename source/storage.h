/*
 * storage.h
 *
 *  Created on: Dec 27, 2019
 *      Authors: Basile Spealehauer and Marine Fumeaux
 */

#ifndef STORAGE_H_
#define STORAGE_H_

//Initialize the values of the score by reading from the file
void init_storage(void);

//Write a new score to the storage device but only if it is higher than the
//previous one
void write_score(int new_score, int new_time);

//Get the stored score
int get_stored_score(void);

//Get the stored time
int get_stored_time(void);

#endif /* STORAGE_H_ */
