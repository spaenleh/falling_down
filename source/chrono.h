/*
 * chrono.h
 *
 *  Created on: Dec 23, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef CHRONO_H_
#define CHRONO_H_

//Update the chronometer every seconds and call a function the print the digits
void chrono_update(int min, int sec);

//Call a function to start the timer 1
void chrono_init();

//Call a function to stop the timer 1
void chrono_stop();

#endif /* CHRONO_H_ */
