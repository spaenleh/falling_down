/*
 * timer_management.c
 *
 *  Created on: Nov 28, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */
#include <nds.h>
#include <stdio.h>

#include "timer_management.h"
#include "parachute.h"
#include "barque.h"
#include "menu.h"
#include "chrono.h"
#include "constantes.h"

// *************** GLOBAL VARIABLES ******************

int timer0_ticks, timer2_ticks;
int sec, min;

// **************** CODE FOR FUNCTIONS *********************

void configureTimer() {
	// Initialize timer_ticks
	timer0_ticks = 0;
	timer2_ticks = 0;
	sec = 0;
	min = 0;
	speed_para = SPEED_DEFAULT_PARA;
	speed_barque = SPEED_DEFAULT_BARQ;

	// Configure timer to trigger an interrupt every 100 ms
	TIMER_DATA(0) = TIMER_FREQ_1024(10);
	TIMER_CR(0) = TIMER_ENABLE | TIMER_DIV_1024 | TIMER_IRQ_REQ;

	// Configure timer to trigger an interrupt every 1s
	TIMER_DATA(1) = TIMER_FREQ_1024(1);
	TIMER_CR(1) = TIMER_ENABLE | TIMER_DIV_1024 | TIMER_IRQ_REQ;

	// Configure timer to trigger an interrupt every 25ms
	// change en fonction de la difficulté
	TIMER_DATA(2) = TIMER_FREQ_1024(40);
	TIMER_CR(2) = TIMER_ENABLE | TIMER_DIV_1024 | TIMER_IRQ_REQ;

	// Associate the ISR (timerISR) to the interrupt line and enable it
	//irqInit();
	irqSet(IRQ_TIMER0, &timer0ISR);
	irqSet(IRQ_TIMER1, &timer1ISR);
	irqSet(IRQ_TIMER2, &timer2ISR);
	irqEnable(IRQ_TIMER0);
	irqEnable(IRQ_TIMER2);

}

void timer0ISR() {
	timer0_ticks++;
	if(timer0_ticks >= speed_para){
		timer0_ticks = 0;
		//new parachute every 1s
		new_para();
	}
}

void timer1ISR(){
	sec++;
	if(sec >= 60){
		min++;
		sec = 0;
	}
	// call the function to update the chrono
	chrono_update(min,sec);
}

void timer2ISR(){
	timer2_ticks++;
	if(timer2_ticks >= speed_barque){
		timer2_ticks = 0;
		barque_update();
	}
}

void timer1Start(){
	irqEnable(IRQ_TIMER1);
	sec = 0;
	min = 0;
	chrono_update(min,sec);
}

void timer1Stop(){
	irqDisable(IRQ_TIMER1);
}

int get_time(){
	return min*100+sec;
}
