/*
 * menu.c
 *
 *  Created on: Dec 19, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */
#include <nds.h>
#include <stdio.h>

#include "menu.h"
#include "constantes.h"
#include "chrono.h"
#include "sound.h"
#include "graphics.h"
#include "score.h"
#include "storage.h"
#include "timer_management.h"

// ************************** LOCAL FUNCTIONS *********************

//All these functions manage the input keys depending on which state is the current one
void menu_input(void);
void playing_input(void);
void dif_input(void);
void high_input(void);
void finished_input(void);


// ************************* CODE FOR FUNCTIONS *********************
void menu_init(){
	state = MENU;
	graphics_MAIN_menu();
	graphics_SUB_menu();
}

void menu_state(void){
	switch (state) {
	case MENU:
		menu_input();
		break;
	case DIF:
		dif_input();
		break;
	case HIGH:
		high_input();
		break;
	case PLAYING:
		playing_input();
		break;
	case FINISHED:
		finished_input();
	default:
		break;
	}
}

void menu_finished(void){
	state=FINISHED;
	chrono_stop();

	graphics_MAIN_gameover();
	graphics_SUB_gameover();
	write_score(get_score(),get_time());
}

// ************** CODE FOR LOCAL FUNCTIONS ******************

void menu_input(void){
	static int pos = -1;
	scanKeys();
	int keys = keysDown();
	if(keys & KEY_DOWN){
		pos += 1;
		if(pos>2)
			pos=0;
	}
	if(keys & KEY_UP){
		pos -= 1;
		if(pos<0)
			pos = 2;
	}
	graphics_menu_select(pos);

	if(keys & KEY_A){
		switch (pos) {
		case 0:
			state=PLAYING;
			graphics_MAIN_playing();
			graphics_SUB_playing();
			break;
		case 1:
			state=DIF;
			graphics_SUB_dif();
			break;
		case 2:
			state=HIGH;
			graphics_SUB_high();
			int score = get_stored_score();
			int s_time = get_stored_time();
			chrono_update(s_time/100, s_time-(s_time/100)*100);
			printScore(score);
			break;
		default:
			break;
		}
	}
	if(keys & KEY_TOUCH){
		touchPosition touch;
		touchRead(&touch);
		if(touch.py>PLAY_TOP && touch.py<PLAY_BOT && touch.px>PLAY_LEFT && touch.px<PLAY_RIGHT){
			state=PLAYING;
			graphics_MAIN_playing();
			graphics_SUB_playing();

		}
		if(touch.py>DIF_TOP && touch.py<DIF_BOT && touch.px>DIF_LEFT && touch.px<DIF_RIGHT){
			state=DIF;
			graphics_SUB_dif();
		}
		if(touch.py>HIGH_TOP && touch.py<HIGH_BOT && touch.px>HIGH_LEFT && touch.px<HIGH_RIGHT){
			state=HIGH;
		}
	}
}

void dif_input(void){
	static int pos = -1;	// pos should be cleared when the state changes
	scanKeys();
	int keys = keysDown();
	if(keys & KEY_DOWN){
		pos += 1;
		if(pos>2)
			pos=0;
	}
	if(keys & KEY_UP){
		pos -= 1;
		if(pos<0)
			pos = 2;
	}
	graphics_menu_select(pos);

	if(keys & KEY_A){
		switch (pos) {
		case 0:
			speed_para = SPEED_DEFAULT_PARA;
			speed_barque = SPEED_DEFAULT_BARQ;
			break;
		case 1:
			speed_para = SPEED_HIGH_PARA;
			speed_barque = SPEED_DEFAULT_BARQ;
			break;
		case 2:
			speed_para = SPEED_HIGH_PARA;
			speed_barque = SPEED_LOW_BARQ;
			break;
		default:
			speed_para = SPEED_DEFAULT_PARA;
			speed_barque = SPEED_DEFAULT_BARQ;
			break;
		}
		pos = -1;
		menu_init();
	}
	if(keys & KEY_B){
		speed_para = SPEED_DEFAULT_PARA;
		speed_barque = SPEED_DEFAULT_BARQ;
		pos = -1;
		menu_init();
	}

	if(keys & KEY_TOUCH){
		touchPosition touch;
		touchRead(&touch);
		if(touch.py>EASY_TOP && touch.py<EASY_BOT && touch.px>EASY_LEFT && touch.px<EASY_RIGHT){
			speed_para = SPEED_DEFAULT_PARA;
			speed_barque = SPEED_DEFAULT_BARQ;
			pos = -1;
			menu_init();
			return;
		}
		if(touch.py>HARD_TOP && touch.py<HARD_BOT && touch.px>HARD_LEFT && touch.px<HARD_RIGHT){
			speed_para = SPEED_HIGH_PARA;
			speed_barque = SPEED_DEFAULT_BARQ;
			pos = -1;
			menu_init();
			return;
		}
		if(touch.py>HCOR_TOP && touch.py<HCOR_BOT && touch.px>HCOR_LEFT && touch.px<HCOR_RIGHT){
			speed_para = SPEED_HIGH_PARA;
			speed_barque = SPEED_LOW_BARQ;
			pos = -1;
			menu_init();
			return;
		}
	}
}

void high_input(void){
	scanKeys();
	int keys = keysDown();
	if(keys & KEY_B){
		menu_init();
	}
}

void playing_input(void){
	scanKeys();
	int keys = keysDown();
	if(keys & KEY_START){
		// reinit the state to menu
		chrono_stop();
		write_score(get_score(),get_time());
		menu_init();
	}
}

void finished_input(){
	scanKeys();
	int keys = keysDown();
	if(keys & KEY_START){
		// reinit the state to menu
		menu_init();
	}
}
