/*
 * constante.h
 *
 *  Created on: Nov 26, 2019
 *      Authors: Basile Spaenlehauer and Marine Fumeaux
 */

#ifndef CONSTANTE_H_
#define CONSTANTE_H_

#define SCREEN_WIDTH	256
#define	SCREEN_HEIGHT	192

//Parachutes
enum Para{PARA_ROUGE, PARA_VERT, PARA_BLEU, PARA_JAUNE};
#define	SPRITE_WIDTH	32
#define	SPRITE_HEIGHT	32
#define PAL_OFS			2
#define NB_C_PARA		5
#define PARA_OAM_OFS	1
#define TAB_PARA_SIZE	12
#define Y_TOL			16
#define X_TOL			16
#define MAX_LIVES		8
#define NO_PARA_HEIGHT	140

//Barque
#define BARQUE_Y		150
#define BARQUE_INDEX	0
#define BARQ_INC		10
#define BARQ_OFS_MIDDLE	35
#define BARQ_OFS_RIGHT	59
#define LIM_LEFT		11
#define LIM_RIGHT		59

//Score
#define DIGI_UNIT		14
#define DIGI_DECI		16
#define DIGI_CENTI      18
#define SCORE_Y			3

//Chrono
#define COL 			-1

//Palette
#define MAIN_PAL_LEN	6
#define SUB_PAL_LEN		8
#define TRANS			0x03E7
#define NOIR			0x0000
#define ROUGE			0x001F
#define BLEU_FOND		0x6A60
#define	ROUGE_OMBRE		0x0018
#define BLANC			0x7FFF
#define VERT			RGB15(0,31,0)
#define BLEU			RGB15(0,31,31)
#define JAUNE			RGB15(31,31,0)

//Custom palette for background
#define I_BLEU_FOND		0
#define I_NOIR			1
#define I_BLANC			5
#define I_ROUGE			6
#define I_ROUGE_OMBRE	7

//Palette subscreen
#define C_MENU_SELECT	ARGB16(1,31,20,5)
#define C_MENU_DESELECT	ARGB16(1,28,18,28)


//Menu
enum Game_States{MENU=0,DIF,HIGH,PLAYING,FINISHED};

#define PLAY_TOP	20
#define PLAY_BOT	59
#define PLAY_LEFT	85
#define PLAY_RIGHT	172

#define DIF_TOP		76
#define DIF_BOT		111
#define DIF_LEFT	31
#define DIF_RIGHT	224

#define HIGH_TOP	130
#define HIGH_BOT	165
#define HIGH_LEFT	38
#define HIGH_RIGHT	220

#define EASY_TOP	20
#define EASY_BOT	59
#define EASY_LEFT	85
#define EASY_RIGHT	172

#define HARD_TOP	76
#define HARD_BOT	111
#define HARD_LEFT	31
#define HARD_RIGHT	224

#define HCOR_TOP	130
#define HCOR_BOT	165
#define HCOR_LEFT	38
#define HCOR_RIGHT	220

#define INDEX_PLAY	9
#define INDEX_DIFF	8
#define INDEX_HIGH	7
#define MENU_INDEX_MAX	3

//Timer
#define SPEED_DEFAULT_PARA	10
#define SPEED_HIGH_PARA		 7
#define SPEED_DEFAULT_BARQ	 3
#define SPEED_LOW_BARQ		 7

#endif /* CONSTANTE_H_ */
