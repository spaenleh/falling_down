/*
 * score.h
 *
 *  Created on: Dec 10, 2019
 *      Authors: Basile Speanlehauer and Marine Fumeaux
 */
#ifndef SCORE_H_
#define SCORE_H_

//Initialize and display the score
void score_init();

//Update and display the score if a collsion occured
void score_update();

//Return the score when the game is over
int get_score();

//Display the digits for the score or the chronometer
void printScore(int number);

#endif /* SCORE_H_ */
